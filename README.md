"""
 Data Science

 Logistic Regression in Python

* Introduction and Outline

    *
    why lr ?

        * fundamental for deep learning
        and complex ml models like GANs
        neural networks and deep belief networks
        * programming = implementing the math
        * do not be afraid
        * how far do you want to go ?

    *
    outline

        * what is classification in the boarder sense of ml


        * biological inspiration
            * neurons
            * perceptrons = older models of neurons

        * logistic regression -- picture / schematic
        * feed-forward mechanism, probabilistic interpretation

        * cross - entropy error function
        * maximum likelihood
            * objective function

        * solve for this objective
            * gradient descent

            * practical problems
                * regularization
                * donut problem
                * XOR problem


* project

    *
    practical data analysis
        * i need to get to know exactly how
        the techniques i learn are used in the
        real world

        * data could be derived from
            * excel spreadsheet
            * sql table
            * log files

    *
    project
        * e-commerce store
        * predict : user actions on web-site
        * direct monetary impact
            * predict bounce
            * discover which areas of the site are weak
            * ex. mobile, user-friendliness
            * make data-driven decisions
            * use science to improve user experience








"""

