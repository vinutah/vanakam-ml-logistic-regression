"""
 * Visualizes
    * Points
    * Bayes solution
"""

"""
* Python Libraries
"""

"""
* X-Data
"""

N = 100
D = 2
import numpy as np
X = np.random.randn(N, D)
X[:50, :] = X[:50, :] - 2*np.ones((50, D))
X[50:, :] = X[50:, :] + 2*np.ones((50, D))
Xb = np.concatenate((np.ones((N, 1)), X), axis=1)

"""
* Y-Data
"""
T = np.array([0]*50 + [1]*50)


"""
* Model
"""


def sigmoid(s):
    return 1/(1+np.exp(-s))
w = np.array([0, 4, 4])
Y = sigmoid(Xb.dot(w))

"""
* Plot
"""
import matplotlib.pyplot as plt
plt.scatter(X[:, 0], X[:, 1], c=T, s=100, alpha=0.5)

x_axis = np.linspace(-6, 6, 100)
y_axis = -x_axis

plt.plot(x_axis, y_axis)
plt.show()

