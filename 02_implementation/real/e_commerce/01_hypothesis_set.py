"""
making logistic predictions
    * or a real data set
"""

"""
X: Data
"""
import _00_input_processing as ip
X, Y = ip.get_binary_data()
D = X.shape[1]

"""
H: Hypothesis Set
"""
import numpy as np
W = np.random.randn(D)
b = 0 # bias term


def sigmoid(a: float) -> float:
    return 1/(1 + np.exp(-a))


def forward(x: np.ndarray, w: np.ndarray, b: float) -> np.ndarray:
    return sigmoid(x.dot(w) + b)

p_y_given_x = forward(X, W, b)
predictions = np.round(p_y_given_x)

"""
M: Accuracy
"""


def classification_rate(y: object, p: object) -> object:
    return np.mean(y == p)

print("Score:", classification_rate(Y, predictions))
