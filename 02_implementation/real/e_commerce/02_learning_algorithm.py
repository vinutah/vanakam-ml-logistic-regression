"""
code for gradient descent of the cross-entropy error
"""


"""
Data
"""
from _00_input_processing import get_binary_data
x, y = get_binary_data()

from sklearn.utils import shuffle
x, y = shuffle(x, y)

"""
Create train and test sets
"""
xtrain = x[:-100]  # with hold 100 points as test set
xtest = x[-100:]  # 100 points are test set

ytrain = y[:-100]  # similar thing for labels
ytest = y[-100:]

"""
Weights
"""
d = x.shape[1]
import numpy as np
w = np.random.randn(d)
b = 0

"""
Predictions
"""


def sigmoid(a: np.ndarray) -> np.ndarray:
    return 1 / (1 + np.exp(-a))


def forward(x: np.ndarray, w: np.ndarray, b: np.ndarray) -> np.ndarray:
    return sigmoid(x.dot(w) + b)


"""
Error
TODO : is mean ok ?
"""


def cross_entropy(t: np.ndarray, py: np.ndarray) -> np.ndarray:
    return -np.mean(t*np.log(py) + (1 - t)*np.log(1 - py))


"""
accuracy
"""


def classification_rate(y: np.ndarray, p: np.ndarray) -> np.ndarray:
    return np.mean(y == p)

"""
train
"""

# main training loop
# goal is to reduce these costs

train_costs = []
test_costs = []

learning_rate = 0.001

for i in range(10000):
    """
    forward pass
    """
    pytrain = forward(xtrain, w, b)
    pytest = forward(xtest, w, b)

    """
    how bad ?
    """
    ctrain = cross_entropy(ytrain, pytrain)
    ctest = cross_entropy(ytest, pytest)

    """
    store the loss
    """
    train_costs.append(ctrain)
    test_costs.append(ctest)

    """
    now ready to desend ... 
    update the weights
    """
    w = w - learning_rate*xtrain.T.dot(pytrain - ytrain)
    b = b - learning_rate*(pytrain - ytrain).sum()

    if i % 1000 == 0:
        print(i, ctrain, ctest)


print("final train classification_rate: {}".format(classification_rate(ytrain, np.round(pytrain))))
print("final test classification_rate: {}".format(classification_rate(ytest, np.round(pytest))))


"""
Visualization
"""

import matplotlib.pyplot as plt
legend1 = plt.plot(train_costs, label='train cost')
legend2 = plt.plot(test_costs, label='test cost')
plt.legend([legend1, legend2])
plt.show()
