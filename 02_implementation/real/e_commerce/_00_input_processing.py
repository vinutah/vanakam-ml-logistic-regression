import os
dir_path = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))


dataset = '/data/ecommerce_data.csv'


def get_data():
    # load
    import pandas as pd
    df = pd.read_csv(dir_path + dataset)
    print(df.head(10))
    data = df.as_matrix()
    x = data[:, :-1]
    y = data[:, -1]

    # normalize
    x[:, 1] = (x[:, 1] - x[:, 1].mean()) / x[:, 1].std()
    x[:, 2] = (x[:, 2] - x[:, 2].mean()) / x[:, 2].std()

    # memory for new features
    n, d = x.shape
    import numpy as np
    x2 = np.zeros((n, d+3))
    x2[:, 0:(d-1)] = x[:, 0:(d-1)]  # copy the non-categoricals

    # features
    for n in range(n):
        t = int(x[n, d-1])  # all rows, last but one column
        x2[n, d-1+t] = 1

    return x2, y


def get_binary_data():
    # return only the data from the first 2 classes
    x, y = get_data()
    x2 = x[y <= 1]
    y2 = y[y <= 1]
    return x2, y2
