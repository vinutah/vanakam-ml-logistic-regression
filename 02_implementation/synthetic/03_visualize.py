"""
* Raw data
    * center the first 50 points at (-2, 2)
    * center the  last 50 points at (-2, 2)
"""
N = 100
D = 2
import numpy as np
X = np.random.randn(N, D)
X[:50, :] = X[:50, :] - 2*np.ones((50, D))
X[50:, :] = X[50:, :] + 2*np.ones((50, D))
Xb = np.concatenate((np.ones((N, 1)), X), axis=1)

"""
* Labels
    * first 50 points are 0
    * last 50 points are 1 
"""
T = np.array([0]*50 + [1]*50)

"""
* Model output
    * need a score
    * need to calculate the sigmoid of the score
"""


def sigmoid(z: np.ndarray) -> np.ndarray:
    isinstance(z, np.ndarray)
    return 1/(1 + np.exp(-z))

# get the closed form of the solution
w = np.array([0, 4, 4])
score = Xb.dot(w)
Y = sigmoid(score)

""""
Visualize
    * Study of plt - In progress
"""
import matplotlib.pyplot as plt
plt.scatter(X[:, 0], X[:, 1], c=T, s=100, alpha=0.5)

x_axis = np.linspace(-6, 6, 100)
y_axis = -x_axis # we know y = -x

plt.plot(x_axis, y_axis)
plt.show()
