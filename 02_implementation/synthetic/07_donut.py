"""
Program to show that logistic regression works on donuts
after appropriate feature transformations
"""

"""
Raw X Data
"""
n = 1000
d = 2

r_inner = 5
r_outer = 10

# distance from the origin is radius
# and then add some noise.
import numpy as np
r1 = r_inner + np.random.randn(n//2)
theta = 2*np.pi*np.random.random(n//2)
x_inner = np.concatenate([[r1 * np.cos(theta)], [r1 * np.sin(theta)]]).T
print("x_inner : {}".format(x_inner))

r2 = r_outer + np.random.randn(n//2)
theta = 2*np.pi*np.random.random(n//2)
x_outer = np.concatenate([[r2 * np.cos(theta)], [r2 * np.sin(theta)]]).T
print("x_outer : {}".format(x_outer))

x = np.concatenate([ x_inner, x_outer])

"""
Raw Y Data
"""
# labels, first 50 are 0 and last 50 are 1
# print(n, type(n))
# print(n//2, type(n//2))
t = np.array([0]*(n//2) + [1]*(n//2))

"""
Visualize
"""
import matplotlib.pyplot as plt
plt.scatter(x[:, 0], x[:, 1], c=t)
plt.show()


"""
Feature Transformation
"""

# add a column of ones
# add a column of sqrt(x^2 + y^2)
ones = np.ones((n, 1))
r = np.zeros((n, 1))
for i in range(n):
    r[i] = np.sqrt(x[i, :].dot(x[i, ]))  # TODO
xb = np.concatenate((ones, r, x), axis=1)

"""
Predictions
"""
w = np.random.standard_normal(d+2)
z = xb.dot(w)


def sigmoid(z: np.ndarray) -> np.ndarray :
    return 1/(1 + np.exp(-z))

y = sigmoid(z)

"""
Error
"""
# calculate the cross-entropy error


def cross_entropy(t: np.ndarray, y: np.ndarray) -> np.ndarray :
    return (t*np.log(y) + (1-t)*np.log(1-y)).sum

"""
Training
"""
learning_rate = 0.0001

# prepare to store error's
error = []

for i in range(5000):

    # how wrong are you now ?
    e = cross_entropy(t, y)

    # save your error
    error.append(e)

    # view it on your terminal
    if i % 100 == 0:
        print(e)

    # gd weight update with regularization
    w = w + learning_rate * (xb.T.dot(t - y) - 0.1*w)

    # recalculate y
    y = sigmoid(xb.dot(w))

"""
Visualization
"""

plt.plot(error)
plt.title('cross-entropy per iteration')
plt.show()

print('final w {}'.format(w))
# TODO these one liners are a hurting me
print('final classification rate {}'.format((1 - np.abs(t - np.round(y)).sum())/n))




















