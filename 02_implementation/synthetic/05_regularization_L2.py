"""
Code to Illustrate L2 regularization
"""

"""
Data
"""

n = 100
d = 2
# x Data
import numpy as np
x = np.random.standard_normal((n, d))
x[:50, :] = (-2) * np.ones((50, d)) + x[:50, :]
x[50:, :] = ( 2) * np.ones((50, d)) + x[50:, :]
xb = np.concatenate((np.ones((n, 1)), x), axis=1)

# target y Data
t = np.array([0]*50 + [1]*50)

"""
Weights
"""
w = np.random.standard_normal(d + 1)

"""
Predictions
"""
score = xb.dot(w)


def sigmoid(a:np.ndarray) -> np.ndarray:
    return 1/(1 + np.exp(-a))

# y is the nd.array that our model predicts
y = sigmoid(score)

"""
Error Function
"""


def cross_entropy(t:np.ndarray, y:np.ndarray) -> np.ndarray:
    e = 0
    for i in range(n):
        if t[i] == 1:
            e = e - np.log(y[i])
        else:
            e = e - np.log(1- y[i])
    return e

"""
Training
"""

learning_rate = 0.1
for i in range(100):
    if i % 10 == 0:
        print(cross_entropy(t, y))

    w = w + learning_rate * ( xb.T.dot(t - y) - 0.1 * w )

    y = sigmoid(xb.dot(w))

print("Final w {}".format(w))

# import matplotlib.pyplot as plt
# plt.scatter(x[:,0], x[:,1], c=t, s=100, alpha=0.5)
# x_axis = np.linspace(-6, 6, 100)
# y_axis = -(w[0] + x_axis*w[1])/w[2]
# plt.plot(x_axis, y_axis)
# plt.show()



