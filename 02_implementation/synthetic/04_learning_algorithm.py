"""
how gradient descent works with numpy matrices
"""

"""
X: Data
"""
n = 100
d = 2
import numpy as np
x = np.random.rand(n, d)
x[:50, :] = x[:50, :] - 2*np.ones((50, d))
x[50:, :] = x[50:, :] + 2*np.ones((50, d))
xb = np.concatenate((np.ones((n, 1)), x), axis=1)


"""
Y: Data
"""
t = np.array([0]*50 + [1]*50)


"""
H: Hypothesis Set
"""
w = np.random.randn(d + 1)

z = xb.dot(w)


def sigmoid(z):
    return 1/(1 + np.exp(-z))

y = sigmoid(z)


"""
E: Error Function
"""


def cross_entropy(t: np.ndarray, y: np.ndarray) -> np.ndarray:
    e = 0
    for i in range(n):
        if 1 == t[i]:
            e = e - np.log(y[i])
        else:
            e = e - np.log(1 - y[i])
    return e


"""
O: Optimization (Minimize Error)
"""
learning_rate = 0.1
for i in range(100):
    if i % 10 == 0:
        print("Error = {}".format(cross_entropy(t, y)))

        w = w + learning_rate * xb.T.dot(t - y)
        y = sigmoid(xb.dot(w))

print("Final w {}".format(w))


"""
V: Visualization
"""
# Plot the data and the separating line
import matplotlib.pyplot as plt
plt.scatter(x[:,0], x[:,1], c=t, s=100, alpha=0.5)
x_axis = np.linspace(-6, 6, 100)
y_axis = -(w[0] + x_axis*w[1])/w[2]
plt.plot(x_axis, y_axis)
plt.show()


