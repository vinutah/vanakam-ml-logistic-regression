"""
How do we calculate the output of a neuron or a
logistic classifier?
"""

"""
X: Data
"""
n = 100
d = 2

import numpy as np

x = np.random.standard_normal((n, d))

x = np.concatenate([np.ones((n, 1)), x], axis=1)

"""
H: Hypothesis Set
# Current Research Problem: Precision Tune these weights
"""
w = np.random.standard_normal(d + 1)

score = x.dot(w)


def sigmoid(s: float) -> float:
    return 1/(1 + np.exp(-s))

p = sigmoid(score)


