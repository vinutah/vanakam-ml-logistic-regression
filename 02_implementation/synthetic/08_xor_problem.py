"""
Logistic units for XOR  problems
Illustrating Manual Feature Engineering
"""

"""
X - Data
"""
n = 4
d = 2
import numpy as np
x = np.array([
    [0, 0],
    [0, 1],
    [1, 0],
    [1, 1],
    ])

"""
Y - Data
"""
t = np.array([0, 1 , 1, 0])

# import matplotlib.pyplot as plt
# plt.scatter(x[:,0], x[:,1], c=t)
# plt.show()

"""
X Data - Transformations
Manual Feature Engineering
Neural Networks and Deep learning Automate this process
The Mathematics Like Chain Rule does this for us
"""
ones = np.ones((n, 1))
xy = ( x[:, 0] * x[:, 1] ).reshape(n, 1)
xb = np.concatenate((ones, xy, x), axis=1)

"""
H: Hypothesis Set
"""
w = np.random.randn(d + 2)
score = xb.dot(w)


def sigmoid(a: np.ndarray) -> np.ndarray :
    return 1/(1 + np.exp(-a))

y = sigmoid(score)

"""
E: Error Function
"""


def cross_entropy(t: np.ndarray, y: np.ndarray) -> np.ndarray :
    e = 0
    for i in range(n):
        if t[i] == 1:
            e = e - np.log(y[i])
        else:
            e = e - np.log(1 - y[i])
    return e

"""
A: Learning Algorithm
"""

learning_rate = 0.001

# prepare to store current error values here
error = []

for i in range(100000):

    e = cross_entropy(t, y)

    error.append(e)

    if i % 100 == 0 :
        print(e)

    # gradient descent weight update with regularization
    w = w + learning_rate * ( xb.T.dot(t - y) - 0.01*w)

    # recalculate y
    y = sigmoid(xb.dot(w))

# import matplotlib.pyplot as plt
# plt.title('cross-entropy per iteration')
# plt.plot(error)
# plt.show()

print('final w {}'.format(w))
print('final classification rate {}'.format( 1 - (np.abs(t - np.round(y)).sum())/n) )