"""
Implementing the Intution and theory
behind L1 regularization
"""

"""
Data
"""

n = 50
d = 50

# Idea is to generate points between -5 and 5
# Uniformely distributed
import numpy as np
x = ( np.random.random((n, d)) - 0.5 ) * 10

# Idea is to setup a sparse dependency on the input data points
# Consider these as the true weights
# And only the first 3 dimensions of X affect Y
true_w = np.array([1, 0.5, -0.5] + [0]*(d - 3))

# Idea is to generate target lables and add some noise
# add noise with variance 0.5


def sigmoid(a):
    return 1/(1 + np.exp(-a))
y = np.round(sigmoid(x.dot(true_w)) + 0.5*np.random.randn(n))

"""
Perform gradient descent to find the weights
"""

# the idea is to keep track of the error function's value
costs = []

# the idea is to randomly initialize the weights
w = np.random.randn(d) / np.sqrt(d)  # why ?

learning_rate = 0.001  # fixed learning rate

l1 = 10.0  # set the hyperparameter value for now

"""
Main training loop
"""
for t in range(5000):
    # update the weights
    # find and store the value of the error function

    yhat = sigmoid(x.dot(w))
    delta = yhat - y
    w = w - learning_rate*(x.T.dot(delta) + l1*np.sign(w))

    cost = -((y)*np.log(yhat) + (1 - y)*np.log(1 - yhat)).mean() + l1*np.abs(w).mean()
    costs.append(cost)

# the idea is to plot 2 costs
import matplotlib.pyplot as plt
plt.plot(costs)
plt.show()

print("final w {}".format(w))

# for the second plot
# the idea is to compare w with true w
plt.plot(true_w, label='true w')
plt.plot(w, label='w_map')
plt.legend()
plt.show()
