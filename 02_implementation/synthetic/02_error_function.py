"""
Data:
* Center the first 50 points at (2, 2)
* Center the first 50 points at (-2, -2)
* Add a column of ones
* Labels: first 50 are 0, last 50 are 1
* TODO : Practice this
"""
N = 100
D = 2
import numpy as np
X = np.random.rand(N, D)
X[:50, :] = (-2)*np.ones((50, D)) + X[:50, :]
X[50:, :] = (+2)*np.ones((50, D)) + X[50:, :]
T = np.array([0]*50 + [1]*50)
Xb = np.concatenate((np.ones((N, 1)), X), axis=1)

"""
Weights
* Randomly initialize the weights
"""
w = np.random.rand(D + 1)

"""
Model
* produce the prediction from the model
"""
score = Xb.dot(w)


def sigmoid(z: np.ndarray) -> np.ndarray:
    return 1/(np.exp(-z)+1)

Y = sigmoid(score)

"""
Error Function
* TODO need to make this functional
* This calculates the cross-entropy error
"""


def cross_entropy(t: np.ndarray, y: np.ndarray) -> np.ndarray:
    e = 0
    for i in range(N):
        if t[i] == 1:
            e = e - np.log(y[i])
        else:
            e = e - np.log(1 - y[i])
    return e

print("cross entropy = {}".format(cross_entropy(T, Y)))


"""
Analytical Solution
"""
w = np.array([0, 4, 4])

"""
 Calculate the model output
"""
score = Xb.dot(w)
Y = sigmoid(score)

"""
Calculate the cross-entropy error
Now it will a lot lower.
"""
print("cross-entropy is {}".format(cross_entropy(T, Y)))
