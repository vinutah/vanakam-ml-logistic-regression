X, Y : Data Source

* Facial Expression Recognition
* Real-world practical example
* https://www.kaggle.com/c/challenges-in-representation-learning-facial-expression-recognition-challenge
    * Free
    * Preprocessed
* Learn facial expressions from an image
  eading emotions from people's face
* ICML 2013

X, Y: Data

* 48 x 48
* Grayscale Images
* Faces
    * Centered
    * Approximately same size
* 7 Classes
    0 - angry
    1 - disgust
    2 - fear
    3 - happy
    4 - sad
    5 - surprise
    6 - neutral
* 3 column csv
    * label
    * pixel
    * space separated
    * train/test    

